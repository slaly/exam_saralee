<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> <!-- viewport : เว็บไซต์แบบ responsive-->
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        c{
            color: red;
        }
    </style>
    <title>Exam5</title>
</head></br>
<body>
    <div class="container">
    <form action ="" method = "POST" id="frmForm" >
    <div class="row">
    <div class="col-sm-1">
    ชื่อ<c>*</c> </div>
    <div class ="col-sm-5"> <input type="text" class="form-control" id="firstname" name="firstname" maxlength=50 placeholder ="กรุณาใส่ชื่อ" data-title="first name"  >
<div class="alert alert-light" role="alert" id="atfname" style="display:none;">
    <c>กรุณาใส่ชื่อ!</c>
</div>
</div>
    <div class="col-sm-1">
    นามสกุล<c>*</c> </div>
    <div class="col-sm-5"><input type="text" class="form-control" id ="lastname" name="lastname" maxlength=50 placeholder ="กรุณาใส่นามสกุล" data-title="last name" > <!--placeholder : ใส่ตัวบางๆที่ ช่องกรอก -->
	<div class="alert alert-light" role="alert" id="atlname" style="display:none;">
    <c>กรุณาใส่นามสกุล!</c>
</div>  </div>
  </div></br>
  <div class="row">
    <div class="col-sm-1">
    ตำแหน่ง<c>*</c></div>
    <div class ="col-sm-5">
      <select class="form-control" id= "position"   data-title="position" >
        <option value="">กรุณาเลือก</option>
        <option value="Developer">Developer</option>
        <option value="SA">SA</option>
        <option value="PM">PM</option>
        </select> 
        <div class="alert alert-light" role="alert" id ="atposition" style="display:none;">
        <c>กรุุณาใส่ตำแหน่ง</c>
    </div></div> </div> </br>
	
	
    <div class="row">
    <div class="col-sm-12" align ="center">
	<!-- ปุ่มบันทึก  -->
    <button type ="button" id ="save" class="btn btn-success">Save</button>
   <!-- ปุ่มเคลียร์  -->
    <button type ="button" id ="clear" class="btn btn-secondary">Clear</button>
  
    </div> 
    </form>

</div>

<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

<script>
    
$(function () {
    $('#save').click(function (e) { 
        e.preventDefault();  
        if(!validate()){

        }
        return true;  //<!--if true give disabled-->
		$('select').attr('disabled', true);  
        $('input').attr('disabled', true);
        $('#save').attr('disabled', true);    
    }); 
	
	$('#clear').click(function (e) { 
        e.preventDefault();

        $('input').val("");  //<!--รับค่า-->
        $('select').val("");
        $('#firstname').focus();
		$('select').removeAttr('disabled'); //<!--เคลียร์ค่า ที่กรอก หรือเลือก -->
        $('#save').removeAttr('disabled'); 
		$('input').removeAttr('disabled');

    });
 
    
})

function validate() {

    $('.form-control').each(function(index, element){
        var v_obj = $(this);
        var v_fname = $('#firstname').val();
        var v_lname = $('#lastname').val();
        var v_position = $('#position').val();
       if(v_obj.val()==""){
                if(v_fname== ""){
                    $('#atfname').show();
                }
                if(v_lname ==""){
                    $('#atlname').show();
                   
                }
                if(v_position == "" ){
                    $('#atposition').show();
                   
                }
       }
       else {
                if(v_fname != ""){
                    $('#atfname').hide();
                }
                if(v_lname != ""){
                    $('#atlname').hide();
                }
                if(v_position != ""){
                    $('#atposition').hide();
                }    
       } 
    });

}

           
</script>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>